package pt.sapo.dynip.hugopinho.cloneorganizze.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;

import pt.sapo.dynip.hugopinho.cloneorganizze.R;
import pt.sapo.dynip.hugopinho.cloneorganizze.config.ConfiguracaoFirebase;
import pt.sapo.dynip.hugopinho.cloneorganizze.helper.Base64Custom;
import pt.sapo.dynip.hugopinho.cloneorganizze.model.Utilizador;

public class CadastroActivity extends AppCompatActivity {

   private EditText campoNome, campoEmail, campoSenha;
   private Button btnCadastrar;
   private static FirebaseAuth autenticacao;
   private Utilizador utilizador;

    public CadastroActivity() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        getSupportActionBar().setTitle("Cadastro");

        campoNome = findViewById(R.id.editNome);
        campoEmail = findViewById(R.id.editEmail);
        campoSenha = findViewById(R.id.editPassword);
        btnCadastrar = findViewById(R.id.btnRegistar);

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String textoNome = campoNome.getText().toString();
                String textoEmail = campoEmail.getText().toString();
                String textSenha = campoSenha.getText().toString();



                //validar se campos foram preenchidos
                if(validarCampos(textoNome,textoEmail,textSenha)){

                    cadastroUtilizador();
                }


            }

        });

    }

    private void cadastroUtilizador() {

        autenticacao = ConfiguracaoFirebase.getFirebaseAutenticacao();

        autenticacao.createUserWithEmailAndPassword(
            utilizador.getEmail(),
            utilizador.getPassword()
        ).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if( task.isSuccessful()){

                    //salvar dados do utilizador na BD

                    //idUtilizador = email em base64
                    String idUtilizador = Base64Custom.codificarBase64( utilizador.getEmail());
                    utilizador.setIdUtilizador(idUtilizador);
                    utilizador.salvar();
                    finish();
                }else{


                    String execao = "";
                    try {
                        throw task.getException();
                    }catch (FirebaseAuthWeakPasswordException e){
                        execao = "Digite uma senha mais forte";
                    }catch (FirebaseAuthInvalidCredentialsException e){
                        execao = "Digite um email válido";
                    }catch (FirebaseAuthUserCollisionException e){
                        execao = "A sua conta já está cadastrada";
                    }catch (Exception e ){
                        execao = "Erro ao registar a sua conta: " + e.getMessage();
                        e.printStackTrace();
                    }

                    Toast.makeText(CadastroActivity.this,execao,Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    public boolean validarCampos(String textoNome, String textoEmail, String textSenha){
        if(!textoNome.isEmpty()){
            if(!textoEmail.isEmpty()){
                if(!textSenha.isEmpty()){
                    utilizador = new Utilizador(textoNome,textoEmail.toLowerCase(),textSenha);
                    return true;
                }else{
                    Toast.makeText(CadastroActivity.this,"Preencha a Password!",Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(CadastroActivity.this,"Preencha o Email!",Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(CadastroActivity.this,"Preencha o Nome!",Toast.LENGTH_SHORT).show();
        }
        return false;
    }


}