package pt.sapo.dynip.hugopinho.cloneorganizze.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import pt.sapo.dynip.hugopinho.cloneorganizze.R;
import pt.sapo.dynip.hugopinho.cloneorganizze.config.ConfiguracaoFirebase;
import pt.sapo.dynip.hugopinho.cloneorganizze.helper.DateCustom;
import pt.sapo.dynip.hugopinho.cloneorganizze.model.Movimentacao;
import pt.sapo.dynip.hugopinho.cloneorganizze.model.Utilizador;

public class ReceitasActivity extends AppCompatActivity {

    private TextInputEditText campoData, campoCategoria, campoDescricao;
    private EditText campoValor;
    private Movimentacao movimentacao;
    private DatabaseReference firebaseRef = ConfiguracaoFirebase.getFirebaseDatabase();
    private FirebaseAuth autenticacao = ConfiguracaoFirebase.getFirebaseAutenticacao();
    private Double receitaTotal =0.00, receitaPreenchida, receitaAtualizada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receitas);

        campoValor = findViewById(R.id.editValor);
        campoData = findViewById(R.id.editData);
        campoCategoria = findViewById(R.id.editCategoria);
        campoDescricao = findViewById(R.id.editDescricao);

        //Preencher data atual
        campoData.setText(DateCustom.dataAtual());

        recuperarReceitaTotal();

    }

    public void salvarReceita(View v){

        if(validarCamposReceita()){

            receitaPreenchida = Double.parseDouble(campoValor.getText().toString());
            String data = campoData.getText().toString();

            movimentacao = new Movimentacao();
            movimentacao.setValor(receitaPreenchida);
            movimentacao.setCategoria(campoCategoria.getText().toString());
            movimentacao.setDescricao(campoDescricao.getText().toString());
            movimentacao.setData(data);
            movimentacao.setTipo("r");

            //A soma do valor preenchido mais o valor recuperado da BD
            receitaAtualizada = receitaTotal + receitaPreenchida;
            atualizarReceitaTotal(receitaAtualizada);

            movimentacao.salvar(data);
            Toast.makeText(ReceitasActivity.this,"Receita adicionada com sucesso!",Toast.LENGTH_LONG).show();
            finish();
        }

    }

    public Boolean validarCamposReceita(){

        String textoValor = campoValor.getText().toString();
        String textoData = campoData.getText().toString();
        String textoCategoria = campoCategoria.getText().toString();
        String textoDescricao = campoDescricao.getText().toString();

        if(!textoValor.isEmpty()){
            if(!textoData.isEmpty()){
                if(!textoCategoria.isEmpty()){
                    if(!textoDescricao.isEmpty()) {
                        return true;
                    }else{
                        Toast.makeText(ReceitasActivity.this,"Preencha o valor!",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(ReceitasActivity.this,"Preencha o valor!",Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(ReceitasActivity.this,"Preencha a data!",Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(ReceitasActivity.this,"Preencha a Categoria!",Toast.LENGTH_SHORT).show();
        }
        return false;
    }


    public void recuperarReceitaTotal(){
        DatabaseReference utilizadorRef = ConfiguracaoFirebase.getIdUtilizadorCodificado( autenticacao.getCurrentUser().getEmail());

        //recuperar campovalor total
        utilizadorRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Utilizador utilizador = snapshot.getValue( Utilizador.class);
                receitaTotal = utilizador.getReceitaTotal();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void atualizarReceitaTotal(Double receita){
        DatabaseReference utilizadorRef = ConfiguracaoFirebase.getIdUtilizadorCodificado( autenticacao.getCurrentUser().getEmail());
        utilizadorRef.child("receitaTotal").setValue(receita);

    }
}