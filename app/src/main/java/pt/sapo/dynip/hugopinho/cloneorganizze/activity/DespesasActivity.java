package pt.sapo.dynip.hugopinho.cloneorganizze.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import pt.sapo.dynip.hugopinho.cloneorganizze.R;
import pt.sapo.dynip.hugopinho.cloneorganizze.config.ConfiguracaoFirebase;
import pt.sapo.dynip.hugopinho.cloneorganizze.helper.Base64Custom;
import pt.sapo.dynip.hugopinho.cloneorganizze.helper.DateCustom;
import pt.sapo.dynip.hugopinho.cloneorganizze.model.Movimentacao;
import pt.sapo.dynip.hugopinho.cloneorganizze.model.Utilizador;

public class DespesasActivity extends AppCompatActivity {

    private TextInputEditText campoData, campoCategoria, campoDescricao;
    private EditText campoValor;
    private Movimentacao movimentacao;
    private DatabaseReference firebaseRef = ConfiguracaoFirebase.getFirebaseDatabase();
    private FirebaseAuth autenticacao = ConfiguracaoFirebase.getFirebaseAutenticacao();
    private Double despesaTotal =0.00, despesaPreenchida, despesaAtualizada ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_despesas);

        campoValor = findViewById(R.id.editValor);
        campoData = findViewById(R.id.editData);
        campoCategoria = findViewById(R.id.editCategoria);
        campoDescricao = findViewById(R.id.editDescricao);

        //Preencher data atual
        campoData.setText(DateCustom.dataAtual());

        recuperarDespesaTotal();

    }

    public void salvarDespesa(View v){

        if(validarCamposDespesa()){

            despesaPreenchida = Double.parseDouble(campoValor.getText().toString());
            String data = campoData.getText().toString();

            movimentacao = new Movimentacao();
            movimentacao.setValor( despesaPreenchida );
            movimentacao.setCategoria(campoCategoria.getText().toString());
            movimentacao.setDescricao(campoDescricao.getText().toString());
            movimentacao.setData(data);
            movimentacao.setTipo("d");

            //A soma do valor preenchido mais o valor recuperado da BD
            despesaAtualizada = despesaTotal + despesaPreenchida;
            atualizarDespesaTotal(despesaAtualizada);

            movimentacao.salvar(data);
            Toast.makeText(DespesasActivity.this,"Despesa adicionada com sucesso!",Toast.LENGTH_LONG).show();
            finish();
        }

    }

    public Boolean validarCamposDespesa(){

        String textoValor = campoValor.getText().toString();
        String textoData = campoData.getText().toString();
        String textoCategoria = campoCategoria.getText().toString();
        String textoDescricao = campoDescricao.getText().toString();

        if(!textoValor.isEmpty()){
            if(!textoData.isEmpty()){
                if(!textoCategoria.isEmpty()){
                    if(!textoDescricao.isEmpty()) {
                        return true;
                    }else{
                        Toast.makeText(DespesasActivity.this,"Preencha o valor!",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(DespesasActivity.this,"Preencha o valor!",Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(DespesasActivity.this,"Preencha a data!",Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(DespesasActivity.this,"Preencha a Categoria!",Toast.LENGTH_SHORT).show();
        }
        return false;
    }


    public void recuperarDespesaTotal(){
        DatabaseReference utilizadorRef = ConfiguracaoFirebase.getIdUtilizadorCodificado( autenticacao.getCurrentUser().getEmail());

        //recuperar campovalor total
         utilizadorRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Utilizador utilizador = snapshot.getValue( Utilizador.class);
                despesaTotal = utilizador.getDespesaTotal();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    public void atualizarDespesaTotal(Double despesa){
        DatabaseReference utilizadorRef = ConfiguracaoFirebase.getIdUtilizadorCodificado( autenticacao.getCurrentUser().getEmail());
       /* String emailUtilizador = autenticacao.getCurrentUser().getEmail();
        String idUtilizador = Base64Custom.codificarBase64(emailUtilizador);
        DatabaseReference utilizadorRef = firebaseRef.child("utilizadores")
                .child(idUtilizador);*/
        utilizadorRef.child("despesaTotal").setValue(despesa);

    }
}