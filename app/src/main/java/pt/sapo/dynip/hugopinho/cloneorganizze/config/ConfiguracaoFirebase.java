package pt.sapo.dynip.hugopinho.cloneorganizze.config;

import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import pt.sapo.dynip.hugopinho.cloneorganizze.helper.Base64Custom;

public class ConfiguracaoFirebase {

    private  static FirebaseAuth autenticacao;
    private  static DatabaseReference firebase;

    //retornar a instancia do FirebaseAuth, para utilizar a mesma durante toda a aplicação
    public static  FirebaseAuth getFirebaseAutenticacao(){

        if(autenticacao == null){
            autenticacao = FirebaseAuth.getInstance();
        }
        return autenticacao;
    }


    public static DatabaseReference getFirebaseDatabase(){
        if(firebase == null){
            firebase = FirebaseDatabase.getInstance().getReference();
        }
        return firebase;
    }

    public static DatabaseReference getIdUtilizadorCodificado( String emailUtilizador){

            String idUtilizador = Base64Custom.codificarBase64(emailUtilizador.toLowerCase());
            DatabaseReference utilizadorRef = firebase.child("utilizadores")
                    .child(idUtilizador);
            return utilizadorRef;
    }




}
