package pt.sapo.dynip.hugopinho.cloneorganizze.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;

import pt.sapo.dynip.hugopinho.cloneorganizze.R;
import pt.sapo.dynip.hugopinho.cloneorganizze.config.ConfiguracaoFirebase;
import pt.sapo.dynip.hugopinho.cloneorganizze.model.Utilizador;

public class LoginActivity extends AppCompatActivity {

    private EditText campoEmail, campoSenha;
    private Button btnLogin;
    private Utilizador utilizador;
    private FirebaseAuth autenticacao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().setTitle("Login");

        campoEmail = findViewById(R.id.editEmail);
        campoSenha = findViewById(R.id.editSenha);
        btnLogin = findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String textoEmail = campoEmail.getText().toString();
                String textSenha = campoSenha.getText().toString();


                if(validarCampos(textoEmail,textSenha)){

                    utilizador = new Utilizador();
                    utilizador.setEmail(textoEmail);
                    utilizador.setPassword(textSenha);

                    validarLogin();

                }

            }
        });

    }

    public void validarLogin(){
        autenticacao = ConfiguracaoFirebase.getFirebaseAutenticacao();
        autenticacao.signInWithEmailAndPassword(
          utilizador.getEmail(),
          utilizador.getPassword()
        ).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    abrirTelaPrincipal();
                }else{
                    String execao = "";
                    try {
                        throw task.getException();
                    }catch (FirebaseAuthInvalidCredentialsException e){
                        execao = "Email e senha nao correspondem ao utilizador registado";
                    }catch (FirebaseAuthInvalidUserException e){
                        execao = "Utilizador não está registado";
                    }catch (Exception e ){
                        execao = "Erro ao registar a sua conta: " + e.getMessage();
                        e.printStackTrace();
                    }
                    Toast.makeText(LoginActivity.this,execao,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    public boolean validarCampos(String textoEmail, String textSenha) {

        if (!textoEmail.isEmpty()) {
            if (!textSenha.isEmpty()) {
                // utilizador = new Utilizador(textoNome,textoEmail,textSenha);
                return true;
            } else {
                Toast.makeText(LoginActivity.this, "Preencha a Password!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(LoginActivity.this, "Preencha o Email!", Toast.LENGTH_SHORT).show();
        }

        return false;
    }

    public void abrirTelaPrincipal(){
        startActivity(new Intent(this, PrincipalActivity.class));
        finish();
    }
}